package com.alvarez.essalud.models;

import java.io.Serializable;

public class AreaSalud implements Serializable {
    private String id;
    private String nombreAreas;
    private String turno;
    private String doctor;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public AreaSalud(){

    }
    public AreaSalud(String nombreAreas){
        this.nombreAreas=nombreAreas;

    }

    public String getNombreAreas() {
        return nombreAreas;
    }

    public void setNombreAreas(String nombreAreas) {
        this.nombreAreas = nombreAreas;
    }
}
