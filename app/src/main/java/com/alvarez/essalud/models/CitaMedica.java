package com.alvarez.essalud.models;

public class CitaMedica {
    String doctor;
    String fecha;
    String turno;
    String Area;
    String uid;
    String usuario;
    String email;
    String horaRegistro;
    String horaCita;

    public CitaMedica(String doctor, String fecha, String turno, String area, String uid, String usuario,String email,String horaRegistro, String horaCita) {
        this.doctor = doctor;
        this.fecha = fecha;
        this.turno = turno;
        Area = area;
        this.uid = uid;
        this.usuario = usuario;
        this.email = email;
        this.horaRegistro=horaRegistro;
        this.horaCita=horaCita;
    }

    public CitaMedica() {
    }

    public String getHoraRegistro() {
        return horaRegistro;
    }

    public void setHoraRegistro(String horaRegistro) {
        this.horaRegistro = horaRegistro;
    }

    public String getHoraCita() {
        return horaCita;
    }

    public void setHoraCita(String horaCita) {
        this.horaCita = horaCita;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
