package com.alvarez.essalud;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class CentroAsistencialActivity extends AppCompatActivity {

    TextView centro1, centro2;
    String titulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_centro_asistencial);
        getSupportActionBar().setTitle("Centro Asistencial");

        centro1= findViewById(R.id.tvCentroUno);
        centro2= findViewById(R.id.tvCentroDos);

        centro1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CentroAsistencialActivity.this, HomeActivity.class);
                titulo="ESSalud Hospital III-Chimbote";
                intent.putExtra("CENTRO", titulo);
                startActivity(intent);
            }
        });
        centro2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intents = new Intent(CentroAsistencialActivity.this, HomeActivity.class);
                titulo="ESSalud Hospital I ConoSur";
                intents.putExtra("CENTRO", titulo);
                startActivity(intents);
            }
        });

    }


}
