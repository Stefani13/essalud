package com.alvarez.essalud;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.alvarez.essalud.Adapters.AreaAdapter;
import com.alvarez.essalud.models.AreaSalud;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    private DatabaseReference mDataBase;
    FirebaseAuth mAuth;
    private TextView txtArea;
    private AreaAdapter areaAdapter;
    private RecyclerView recyclerView;
    private String CENTRO;
    private String apellido;

    AreaSalud ars;
    private ArrayList<AreaSalud> areaSalud= new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Bundle datos = this.getIntent().getExtras();

        CENTRO = datos.getString("CENTRO");
        getSupportActionBar().setTitle(CENTRO);
        txtArea=findViewById(R.id.txtArea);
        recyclerView= findViewById(R.id.rcAreas);

        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        //Nodo Principal del proyecto de Firebase
        mDataBase= FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        String id = mAuth.getCurrentUser().getUid();
        //Objeto que queremos obtener child
       /* mDataBase.child("User").child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                //snapshot es el objeto "usuario"
                //si el objeto al que estamos haciendo referencia existe
                if (snapshot.exists()){

                    String nombre=snapshot.child("nombre").getValue().toString();
                    int edad=Integer.parseInt(snapshot.child("edad").getValue().toString());
                            txtArea.setText("El nombre es: "+nombre+" la edad: "+edad);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });*/
        getAreaList();

    }
    private void getAreaList(){
        //String id = mAuth.getCurrentUser().getUid();
        //Usamos la referencia de DataBase de Firebase para recuperar los datos
        //child son los hijos del nodo principal
        mDataBase.child("Areas").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    areaSalud.clear();
                    for(DataSnapshot ds:snapshot.getChildren()){
                        //get Key para obtener las areas, debido a que las areas son los id en la base de datos
                        String areaMedica=ds.getKey().toString();
                        //agregamos  los datos obtenidos de Firebase
                        areaSalud.add(new AreaSalud(areaMedica));
                        //ng=areaMedica;
                    }
                    areaAdapter= new AreaAdapter(areaSalud,HomeActivity.this);
                    //poblamos el adapter con los datos obtenidos
                    recyclerView.setAdapter(areaAdapter);

                }
            }
            //Si en caso hay un error
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

   /* @Override
    public void onClick(View view) {
        if (view.getId() == R.id.lyFondo){
                //AreaSalud area=  ;
            AreaSalud current =(AreaSalud) view.getTag();
            String nombf=ng;
                Intent intent = new Intent(this, TurnosActivity.class);
                //intent.putExtra("variable_integer", objeto.getId());
                intent.putExtra("variable_string", nombf);
                //intent.putExtra("objeto_float", objeto.getPrecio());
                startActivity(intent);
                //break;
        }

       /* if (R.id.lyFondo == view.getId()) {
           AreaSalud area= (AreaSalud) view.getTag();
            Intent intent = new Intent(this, TurnosActivity.class);
            //intent.putExtra("variable_integer", objeto.getId());
            intent.putExtra("variable_string", area.getNombreAreas());
            //intent.putExtra("objeto_float", objeto.getPrecio());
            startActivity(intent);
        }
    }*/
}
