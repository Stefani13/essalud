package com.alvarez.essalud;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toolbar;

import com.google.firebase.auth.FirebaseAuth;

public class DetalleCitaMedicaActivity extends AppCompatActivity {
    FirebaseAuth mAuth;
    private Button btnCerrarSesion;
    //Variables para recuperar los extras enviados
    private String NOMBRE, EMAIL, DOCTOR, TURNO, AREA, FECHA, HORA_CITA, HORA_REGISTRO;
    //Componentes para mostrar los datos recuperados
    private TextView nombre, emial, doctor, turno, area, fecha, hora_Cita, hora_registro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_cita_medica);
        getSupportActionBar().setTitle("Detalle Cita Medica");

        mAuth = FirebaseAuth.getInstance();
        btnCerrarSesion= findViewById(R.id.btnCerrarSesion);

        /**
         * Recuperamos los datos de los Extras
         */
        Bundle datos = this.getIntent().getExtras();
        NOMBRE = datos.getString("USUARIO");
        EMAIL = datos.getString("EMAIL");
        DOCTOR = datos.getString("DOCTOR");
        TURNO = datos.getString("TURNO");
        AREA = datos.getString("AREA");
        FECHA = datos.getString("FECHA");
        HORA_CITA = datos.getString("HORA_CITA");
        HORA_REGISTRO = datos.getString("HORA_REGISTRO");

        //Inicializamos los componentes
        nombre= findViewById(R.id.textView2);
        emial= findViewById(R.id.textView4);
        doctor= findViewById(R.id.textView6);
        fecha= findViewById(R.id.textView8);
        turno= findViewById(R.id.textView10);
         area= findViewById(R.id.textView12);
         hora_Cita=findViewById(R.id.textView14);
        hora_registro=findViewById(R.id.textView16);
        /**
         * Mostramos los datos Recuperados
         */
         nombre.setText(NOMBRE);
        emial.setText(EMAIL);
        doctor.setText(DOCTOR);
        fecha.setText(FECHA);
        turno.setText(TURNO);
        area.setText(AREA);
        hora_Cita.setText(HORA_CITA);
        hora_registro.setText(HORA_REGISTRO);


        /**
         * Cerramos Sesion
         */
        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAuth.signOut();
                startActivity(new Intent(DetalleCitaMedicaActivity.this,LoginActivity.class));
                finish();
            }
        });

    }


}
