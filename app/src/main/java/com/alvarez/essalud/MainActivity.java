package com.alvarez.essalud;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import static android.widget.Toast.LENGTH_SHORT;

public class MainActivity extends AppCompatActivity {
    //Creamos los EditText
    private EditText txtNombre, txtApellido, txtDni, txtEdad, txtEmail, txtPassword;
    //Creamos el Boton
    private Button btnRegistrar;

    //Creamos variables de los datos que vamos a registrar
    private String nombre, apellido, dni, edad, email, password;
    //Creamos el objeto de firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Instanciamos los editText o inicializamos

        txtNombre=findViewById(R.id.txtNombre);
        txtApellido=findViewById(R.id.txtApellido);
        txtDni= findViewById(R.id.txtDNI);
        txtEdad= findViewById(R.id.txtxEdad);
        txtEmail=findViewById(R.id.txtEmal);
        txtPassword= findViewById(R.id.txPassword);
        btnRegistrar= findViewById(R.id.btnRegistrar);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        mDatabase= FirebaseDatabase.getInstance().getReference();


        //Añadimos al boton el evento
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nombre=txtNombre.getText().toString();
                apellido=txtApellido.getText().toString();
                dni=txtDni.getText().toString();
                edad=txtEdad.getText().toString();
                email=txtEmail.getText().toString();
                password=txtPassword.getText().toString();


                //Validar si el usuario ingreso valores a los editText
                if (!nombre.isEmpty() && !apellido.isEmpty() && !dni.isEmpty() && !edad.isEmpty() && !email.isEmpty() && !password.isEmpty()){

                    if (password.length()>=6){
                        registrarUsuario();
                    }else {
                        Toast.makeText(MainActivity.this, "La contraseña debe tener al menos 6 caracteres", Toast.LENGTH_SHORT).show();
                    }

                }else{
                    Toast.makeText(MainActivity.this, "Debe completar Campos", Toast.LENGTH_SHORT).show();
                }



            }
        });


    }

    private void registrarUsuario() {
        mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                //validamos si la tarea fue exitosa
                if (task.isSuccessful()){
                    //Mapa de valores para setValue
                    Map<String, Object> map=new HashMap<>();
                    //Poner los campos que queremos en la base de datos
                    map.put("nombre",nombre);
                    map.put("apellido",apellido);
                    map.put("dni",dni);
                    map.put("edad",edad);
                    map.put("email",email);
                    map.put("password",password);

                    //btener el id de firebase cuando usario se registra en el modo autenticacion
                    String id = mAuth.getCurrentUser().getUid();
                    //pasamos el objeto de valores map
                    mDatabase.child("User").child(id).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        //tk2 es la tarea para crear datos en la base de datos
                        public void onComplete(@NonNull Task<Void> task2) {
                            if (task2.isSuccessful()){
                                startActivity(new Intent(MainActivity.this,LoginActivity.class));
                                finish();

                            }else {
                                Toast.makeText(MainActivity.this, "No se pudo crear los Datos Correcteamente", Toast.LENGTH_SHORT).show();

                            }

                        }
                    });

                }
                else{
                    Toast.makeText(MainActivity.this, "No se pudo registrar el usuario", Toast.LENGTH_SHORT).show();

                }
            }
        });

    }
}
