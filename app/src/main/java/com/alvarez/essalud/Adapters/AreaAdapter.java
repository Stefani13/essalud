package com.alvarez.essalud.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alvarez.essalud.HomeActivity;
import com.alvarez.essalud.R;
import com.alvarez.essalud.TurnosActivity;
import com.alvarez.essalud.models.AreaSalud;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
/*
AreaAdapter para mostrar los items del Home
 */
public class AreaAdapter extends RecyclerView.Adapter<AreaAdapter.AreaViewHolder>{

    private ArrayList<AreaSalud> areaSaluds;
    private Context mcontext;
    //private View.OnClickListener listener;

    public AreaAdapter(ArrayList<AreaSalud> recyclerDataArrayList, Context mcontext) {
        this.areaSaluds = recyclerDataArrayList;
        this.mcontext = mcontext;
        //this.listener=listener;
    }


    @NonNull
    @Override
    public AreaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Inflate Layout
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_area, parent, false);
        return new AreaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AreaViewHolder holder, int position) {
        final AreaSalud current = areaSaluds.get(position);
        holder.nombreArea.setText(current.getNombreAreas());

        //holder.lyFondo.getTag();
        holder.lyFondo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /**
                 * Enviamos el nombre del Area a otra actividad mediante el intent
                 */
                Intent intent = new Intent(mcontext, TurnosActivity.class);
                intent.putExtra("variable_string", current.getNombreAreas());
                //intent.putExtra("objeto_float", objeto.getPrecio());
               mcontext.startActivity(intent);

            }
        });
        //holder.itemView.setOnClickListener(listener);
       // holder.lyFondo=holder.itemView.setOnClickListener(listener);

    }

    @Override
    public int getItemCount() {
        return areaSaluds.size();
    }

    public class AreaViewHolder extends RecyclerView.ViewHolder {

        private TextView nombreArea;
        private RelativeLayout lyFondo;
        private ImageView courseIV;


        public AreaViewHolder(@NonNull View itemView) {
            super(itemView);
            nombreArea = itemView.findViewById(R.id.txtAreaTitul);
            lyFondo=itemView.findViewById(R.id.lyFondo);
        }
    }
}