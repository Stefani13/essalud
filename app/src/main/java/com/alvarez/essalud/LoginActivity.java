package com.alvarez.essalud;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity {
    //Creamos los componentes que estan en la vista de login
    private Button btnLogin;
    private EditText email, pass;
    private TextView registrarse;
    //Creamos variables que usaremos para el inicio de sesion/ enviar los datos
    private String usuaro, password;
    //Las instancias que se usaran para el inicio de sesion
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //Inicializamos los componentes
        email= findViewById(R.id.txtEmail);
        pass= findViewById(R.id.txtpass);

        btnLogin= findViewById(R.id.btnLogin);
        registrarse= findViewById(R.id.tvRegistrarse);
        //Inicializamos las instancias
        mAuth = FirebaseAuth.getInstance();
        mDatabase= FirebaseDatabase.getInstance().getReference();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Capturamos los datos de los editText
                usuaro=email.getText().toString();
                password= pass.getText().toString();

                //Validamos que los datos no sean nulos o vacios

                if (!usuaro.isEmpty() && !password.isEmpty()){
                    //Si es correcto llamamos al metodo loginUser()
                    loginUser();

                }else {
                    //De lo contrario Mensaje de Error
                    Toast.makeText(LoginActivity.this, "Complete los Campos", Toast.LENGTH_SHORT).show();
                }

            }
        });

        //Nos envia al registro de usuario
        registrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });


    }

    private void loginUser() {
        //Iniciamos el login usando las instancias y metodos que proporciona firebase para el inicio de Sesion
        mAuth.signInWithEmailAndPassword(usuaro,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    //Si es Exitoso nos enviara la pantalla de Seleccion de Areas
                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    //De lo contrario mensaje de error
                    Toast.makeText(LoginActivity.this, "No se pudo iniciar Sesion, compruebe los datos", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    /**
     * El siguiente metodo es para validar si el usario ha iniciado sesion, si es correcto, nos enviara directamente a la pantalla de Area
     */
    @Override
    protected void onStart() {
        super.onStart();
        //Si el usario esta activo
        if (mAuth.getCurrentUser()!=null){
            startActivity(new Intent(LoginActivity.this,CentroAsistencialActivity.class));
            finish();
        }
    }
}
