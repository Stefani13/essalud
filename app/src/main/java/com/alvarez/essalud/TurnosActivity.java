package com.alvarez.essalud;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.alvarez.essalud.models.AreaSalud;
import com.alvarez.essalud.models.CitaMedica;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class TurnosActivity extends AppCompatActivity implements View.OnClickListener {
    private DatabaseReference mDataBase;
    FirebaseAuth mAuth;
    //Creamos el Boton
    private Button btnAceptar, btnTurno1, btnTurno2;
    private TextView doctor;

    //Variables para enviar al detalle
    private String fecha, doct, turnos, maniana, tarde, usuario, email, apellido, apenomb, horaRegis, horaCitasMedica;


    //Hora Cita
    private static final String CERO = "0";
    private static final String DOS_PUNTOS = ":";
    private ImageView reloj;
    private EditText horas;
    final Calendar date = Calendar.getInstance();
    //Variables para obtener la hora hora
    final int hora = date.get(Calendar.HOUR_OF_DAY);
    final int minuto = date.get(Calendar.MINUTE);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_turnos);
        getSupportActionBar().setTitle("Registro Cita Medica");

        final CalendarView calendarView=findViewById(R.id.cale);

        final TextView Fecha= findViewById(R.id.Fecha);
        fecha=Fecha.getText().toString();
        //Fecha.setText(curDates);
        //curDate = sdf.format(date.getTime());

        /**
         * Recuperamos el dato enviado del AreaAdapter
         */
        Bundle datos = this.getIntent().getExtras();
        final String areauna = datos.getString("variable_string");
        final TextView df = findViewById(R.id.Nombre);
        df.setText(areauna);

        //Hora
        horas= findViewById(R.id.HoraCita);
        reloj=findViewById(R.id.relojCita);

        btnTurno1 = findViewById(R.id.btnTurno1);
        btnTurno2 = findViewById(R.id.btnTurno2);
        btnAceptar = findViewById(R.id.btnAceptar);

        doctor= findViewById(R.id.txtDoctor);
        //capturamos el valor de editText y lo pasamos a la variable
        doct=doctor.getText().toString();

        //Nodo Principal del proyecto de Firebase
        mDataBase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        String id = mAuth.getCurrentUser().getUid();

        if (areauna.equals("Ginecologia")){
            mDataBase.child("Areas").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull final DataSnapshot snapshot) {
                    //snapshot es el objeto "usuario"
                    //si el objeto al que estamos haciendo referencia existe
                    if (snapshot.exists()) {
                        maniana = snapshot.child("Ginecologia").child("Turno1").getValue().toString();
                        tarde= snapshot.child("Ginecologia").child("Turno2").getValue().toString();
                        btnTurno1.setText(maniana);
                        btnTurno2.setText(tarde);
                        btnTurno1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String nomDoctor = snapshot.child("Ginecologia").child("Doctor1").getValue().toString();
                                //Obtenemos los datos para enviar al detalle
                                doctor.setText(nomDoctor);
                                doct=nomDoctor;
                                turnos=maniana;

                            }
                        });
                        btnTurno2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String nomDoctor = snapshot.child("Ginecologia").child("Doctor2").getValue().toString();

                                doctor.setText(nomDoctor);
                                doct=nomDoctor;
                                turnos=tarde;

                            }
                        });



                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

        }else if(areauna.equals("Medicina General")){
            mDataBase.child("Areas").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull final DataSnapshot snapshot) {
                    //snapshot es el objeto "usuario"
                    //si el objeto al que estamos haciendo referencia existe
                    if (snapshot.exists()) {
                        maniana = snapshot.child("Medicina General").child("Turno1").getValue().toString();
                        tarde= snapshot.child("Medicina General").child("Turno2").getValue().toString();
                        btnTurno1.setText(maniana);
                        btnTurno2.setText(tarde);
                        btnTurno1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String nomDoctor = snapshot.child("Medicina General").child("Doctor1").getValue().toString();

                                doctor.setText(nomDoctor);
                                doct=nomDoctor;
                                turnos=maniana;
                            }
                        });
                        btnTurno2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String nomDoctor = snapshot.child("Medicina General").child("Doctor2").getValue().toString();

                                doctor.setText(nomDoctor);
                                doct=nomDoctor;
                                turnos=tarde;
                            }
                        });



                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }else if(areauna.equals("Odontologia")){
            mDataBase.child("Areas").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull final DataSnapshot snapshot) {
                    //snapshot es el objeto "usuario"
                    //si el objeto al que estamos haciendo referencia existe
                    if (snapshot.exists()) {
                        maniana= snapshot.child("Odontologia").child("Turno1").getValue().toString();
                        tarde= snapshot.child("Odontologia").child("Turno2").getValue().toString();
                        btnTurno1.setText(maniana);
                        btnTurno2.setText(tarde);
                        btnTurno1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String nomDoctor = snapshot.child("Odontologia").child("Doctor1").getValue().toString();

                                doctor.setText(nomDoctor);
                                doct=nomDoctor;
                                turnos=maniana;
                            }
                        });
                        btnTurno2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String nomDoctor = snapshot.child("Odontologia").child("Doctor2").getValue().toString();

                                doctor.setText(nomDoctor);
                                doct=nomDoctor;
                                turnos=tarde;
                            }
                        });



                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }else if(areauna.equals("Oftalmologia")){
            mDataBase.child("Areas").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull final DataSnapshot snapshot) {
                    //snapshot es el objeto "usuario"
                    //si el objeto al que estamos haciendo referencia existe
                    if (snapshot.exists()) {
                        maniana= snapshot.child("Oftalmologia").child("Turno1").getValue().toString();
                        tarde= snapshot.child("Oftalmologia").child("Turno2").getValue().toString();
                        btnTurno1.setText(maniana);
                        btnTurno2.setText(tarde);
                        btnTurno1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String nomDoctor = snapshot.child("Oftalmologia").child("Doctor1").getValue().toString();
                                //String nomDos= snapshot.child("Ginecologia").child("Turno2").getValue().toString();
                                doctor.setText(nomDoctor);
                                doct=nomDoctor;
                                turnos=maniana;

                            }
                        });
                        btnTurno2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String nomDoctor = snapshot.child("Oftalmologia").child("Doctor2").getValue().toString();
                                //String nomDos= snapshot.child("Ginecologia").child("Turno2").getValue().toString();
                                doctor.setText(nomDoctor);
                                doct=nomDoctor;
                                turnos=tarde;

                            }
                        });



                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }else if(areauna.equals("Pediatria")){
            mDataBase.child("Areas").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull final DataSnapshot snapshot) {
                    //snapshot es el objeto "usuario"
                    //si el objeto al que estamos haciendo referencia existe
                    if (snapshot.exists()) {
                       maniana = snapshot.child("Pediatria").child("Turno1").getValue().toString();
                        tarde= snapshot.child("Pediatria").child("Turno2").getValue().toString();
                        btnTurno1.setText(maniana);
                        btnTurno2.setText(tarde);
                        btnTurno1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String nomDoctor = snapshot.child("Pediatria").child("Doctor1").getValue().toString();
                                doctor.setText(nomDoctor);
                                doct=nomDoctor;
                                turnos=maniana;
                            }
                        });
                        btnTurno2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String nomDoctor = snapshot.child("Pediatria").child("Doctor2").getValue().toString();
                                doctor.setText(nomDoctor);
                                doct=nomDoctor;
                                turnos=tarde;
                            }
                        });



                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
        /*final Calendar date = Calendar.getInstance();
        //Variables para obtener la hora hora
        final int hora = date.get(Calendar.HOUR_OF_DAY);
        final int minuto = date.get(Calendar.MINUTE);*/

        //iniciamos el calendario
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
                //Obtenemos la fecha en formato String
                String curDate = String.valueOf(dayOfMonth+"/"+(month+1)+"/"+year);
                //Para obtener la fecha actual

                try{
                    //Para convertir los String a Date
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    //Fecha de Hoy
                    String hoy = formatter.format(date.getTime());
                    String str1 =hoy;
                    Date date1 = formatter.parse(str1);

                    //Fecha Elegida o Seleccionada del calendario
                    String str2 =curDate ;
                    Date date2 = formatter.parse(str2);

                    //seleccion de dias, para contar las veces que se eligio fecha
                    int contador=0;

                    //Condicion para compara que la fecha elegida no sea menor a la fecha actual
                    if (date2.after(date1)) {
                        //Si la fecha es correcta se mostrar en el TexView
                        Fecha.setText(curDate);
                        fecha=curDate;
                        contador++;
                    }else{
                        //Si es incorrecta mensaje de error y limpiamos los Texto
                        Toast.makeText(TurnosActivity.this, "Fecha invalida, Elija una fecha despues de la de Hoy", Toast.LENGTH_SHORT).show();
                        Fecha.setText("");
                        fecha="";
                    }
                    /**
                     *Si el contador es mayor a 0 limpiamos la seleccion para elegir otra fecha,
                     *  es en caso se elija una fecha incorrecta o se quiera cambiar de fecha
                     */
                    if (contador>0){
                        calendarView.clearFocus();

                    }


                }catch (ParseException e1){
                    e1.printStackTrace();
                }

            }
        });
        //Recuperamos los datos del usurio para guardar en el registro de la cita
        String uid = mAuth.getCurrentUser().getUid();
        mDataBase.child("User").child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    usuario=snapshot.child("nombre").getValue().toString();
                    apellido= snapshot.child("apellido").getValue().toString();
                    apenomb= usuario+" "+apellido;
                     email=snapshot.child("email").getValue().toString();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        horaRegis= simpleDateFormat.format(new Date());

        reloj.setOnClickListener(this);

        /**
         * Registramos los datos elegidos
         */

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fecha.isEmpty() ){
                    Toast.makeText(TurnosActivity.this, "Ingrese Fecha", Toast.LENGTH_SHORT).show();
                }else if (doct.isEmpty()){
                    Toast.makeText(TurnosActivity.this, "No Eligio Turno", Toast.LENGTH_SHORT).show();
                }else{
                    CitaMedica citaMedica= new CitaMedica();
                    /**
                     *
                     * Obtenemos los datos a registrar y enviamos al registro de Firebase
                     */
                    citaMedica.setUid(UUID.randomUUID().toString());
                    citaMedica.setArea(areauna);
                    citaMedica.setDoctor(doct);
                    citaMedica.setFecha(fecha);
                    citaMedica.setTurno(turnos);
                    citaMedica.setUsuario(apenomb);
                    citaMedica.setEmail(email);
                    citaMedica.setHoraRegistro(horaRegis);
                    citaMedica.setHoraCita(horaCitasMedica);

                    /**
                     * Enviamos los datos a la vista del detalle
                     */
                    mDataBase.child("CitaMedica").child(citaMedica.getUid()).setValue(citaMedica);
                    Intent intent = new Intent(TurnosActivity.this, DetalleCitaMedicaActivity.class);
                    intent.putExtra("AREA", areauna);
                    intent.putExtra("DOCTOR", doct);
                    intent.putExtra("FECHA", fecha);
                    intent.putExtra("TURNO", turnos);
                    intent.putExtra("USUARIO", apenomb);
                    intent.putExtra("EMAIL", email);
                    intent.putExtra("HORA_REGISTRO",horaRegis);
                    intent.putExtra("HORA_CITA",horaCitasMedica);
                    startActivity(intent);
                    finish();

                }
            }
        });



    }


    @Override
    public void onBackPressed() {
        //Si llamas super.onBackPressed(), esto internamente ejecuta finish().
        super.onBackPressed();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.relojCita:
                obtenerHora();
                break;
        }


    }
    private void obtenerHora(){
        TimePickerDialog recogerHora = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                //Formateo el hora obtenido: antepone el 0 si son menores de 10
                String horaFormateada =  (hourOfDay < 10)? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
                //Formateo el minuto obtenido: antepone el 0 si son menores de 10
                String minutoFormateado = (minute < 10)? String.valueOf(CERO + minute):String.valueOf(minute);
                //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario
                String AM_PM;
                if(hourOfDay < 12) {
                    AM_PM = "a.m.";
                } else {
                    AM_PM = "p.m.";
                }
                //Muestro la hora con el formato deseado
                horas.setText(horaFormateada + DOS_PUNTOS + minutoFormateado + " " + AM_PM);
                horaCitasMedica=horaFormateada + DOS_PUNTOS + minutoFormateado + " " + AM_PM;
            }
            //Estos valores deben ir en ese orden
            //Al colocar en false se muestra en formato 12 horas y true en formato 24 horas
            //Pero el sistema devuelve la hora en formato 24 horas
        }, hora, minuto, false);

        recogerHora.show();
    }
}
